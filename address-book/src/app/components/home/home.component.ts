import { Component, OnInit } from '@angular/core';
import { JarwisService } from 'src/app/Services/jarwis.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public form = {
    name: '',
    email: '',
    address: '',
    mobile: ''
  }

  public records: any;
  public error: any;
  public success: any;
  public selectedRecord: any;

  constructor(
    private jarwis: JarwisService
  ) { }

  onSubmit() {
    this.jarwis.create(this.form).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error),
    );
  }

  onEdit() {

    let myFormData = new FormData();
    myFormData.append('selectedRecord', this.selectedRecord);
    myFormData.append('name', this.form.name);
    myFormData.append('email', this.form.email);
    myFormData.append('address', this.form.address);
    myFormData.append('mobile', this.form.mobile);

    this.jarwis.update(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error),
    );
  }

  deleteRecord(id:any){
    this.jarwis.delete({"id": id}).subscribe(
      data => console.log("deleted")
      );
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {
    //read
    this.jarwis.read().subscribe((response: any) => {
      this.records = response['records'];
      // console.log(this.records);
    })

  }

}
