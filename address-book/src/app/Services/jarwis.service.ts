import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class JarwisService {

  private url = environment.url;

  constructor(
    private http: HttpClient,
  ) { }

  create(data: any) {
    return this.http.post(`${this.url}/create.php`, data);
  }

  read() {
    return this.http.get(`${this.url}/read.php`)
  }

  update(data: any) {
    return this.http.post(`${this.url}/update.php`, data);
  }

  delete(data: any) {
    return this.http.post(`${this.url}/delete.php`, data);
  }

}
